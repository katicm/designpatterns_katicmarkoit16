package app;

import java.awt.Graphics;
import java.util.Iterator;

import javax.swing.JPanel;

import shapes.Shapes;

public class PaintBetaView extends JPanel {
	private PaintBetaModel model;

	public void setModel(PaintBetaModel model) {
		this.model = model;
	}

	public void paint(Graphics gc) {
		super.paint(gc);
		if (model != null) {
			Iterator<Shapes> it = model.getAll().iterator();
			while (it.hasNext()) {
				it.next().paint(gc);
			}
		}
	}
}
