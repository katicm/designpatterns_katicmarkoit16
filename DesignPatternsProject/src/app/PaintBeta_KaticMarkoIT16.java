package app;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.UIManager;

import observer.ObserverButton;

public class PaintBeta_KaticMarkoIT16 {

	public static void main(String[] args) {

		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PaintBetaModel model = new PaintBetaModel();
					PaintBetaFrame frame = new PaintBetaFrame();
					frame.getView().setModel(model);
					PaintBetaController controller = new PaintBetaController(model, frame);
					frame.setController(controller);
					ObserverButton observerButton = new ObserverButton(frame);
					model.addObserver(observerButton);
					frame.setSize(900, 600);
					frame.setVisible(true);
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame.setTitle("Java Paint 2.0 Version  powered by KaticM.");
					frame.setBounds(300, 300, 900, 630);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
