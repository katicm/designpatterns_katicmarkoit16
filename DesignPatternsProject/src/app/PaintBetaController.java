package app;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import adapter.HexagonAdapter;
import command.Command;
import command.CommandAddShape;
import command.CommandBringToBack;
import command.CommandBringToFront;
import command.CommandModifyShape;
import command.CommandRemoveShape;
import command.CommandSelectShape;
import command.CommandToBack;
import command.CommandToFront;
import command.CommandUnselectShape;
import dialogs.CircleAddDialog;
import dialogs.CircleModifyDialog;
import dialogs.DialogForm;
import dialogs.HexagonAddDialog;
import dialogs.HexagonModifyDialog;
import dialogs.LineModifyDialog;
import dialogs.PointModifyDialog;
import dialogs.RectangleAddDialog;
import dialogs.RectangleModifyDialog;
import dialogs.SquareAddDialog;
import dialogs.SquareModifyDialog;
import dialogs.TriangleModifyDialog;
import hexagon.Hexagon;
import shapes.Circle;
import shapes.Line;
import shapes.Point;
import shapes.Rectangle;
import shapes.Shapes;
import shapes.Square;
import shapes.Triangle;
import strategy.OpenFileManager;
import strategy.OpenViaImage;
import strategy.OpenViaLog;
import strategy.SaveFileManager;
import strategy.SaveViaImage;
import strategy.SaveViaLog;

public class PaintBetaController {

	private PaintBetaModel model;
	private PaintBetaFrame frame;

	private Stack<Command> undoStack;
	private Stack<Command> redoStack;
	private Queue<String> fromLogs;
	private Line oldLine; // triangle
	private Color strokeColor;
	private Color fillColor;
	private String mode;

	public PaintBetaController(PaintBetaModel model, PaintBetaFrame frame) {
		this.model = model;
		this.frame = frame;
		undoStack = new Stack<Command>();
		redoStack = new Stack<Command>();
		fromLogs = new LinkedList<String>();
		oldLine = new Line(new Point(-1, -1), new Point(-1, -1));
		strokeColor = Color.BLACK;
		fillColor = Color.WHITE;
		mode = "point";
	}

	public int[] getStackSize() {
		int[] count = new int[2];
		count[0] = undoStack.size();
		count[1] = redoStack.size();
		return count;
	}

	public void setMode(String mode) {
		this.mode = mode;
		model.notifyObservers(mode, getStackSize(), fromLogs.size());
	}

	public void resetOldLine() {
		oldLine.setStart(new Point(-1, -1));
		oldLine.setEnd(new Point(-1, -1));
	}

	public void mouseClicked(MouseEvent arg0) {
		Point temp = new Point(arg0.getX(), arg0.getY());
		if (mode == "select" || mode == "delete" || mode == "modify") {
			shouldSelectShape(temp);
		} else {
			whatToDraw(temp);
		}
		frame.getView().repaint();
	}

	public void whatToDraw(Point temp) {
		switch (mode) {
		case "point": {
			pushOnStack(new CommandAddShape(model, new Point(temp.getX(), temp.getY(), strokeColor)));
			break;
		}
		case "line": {
			if (oldLine.getStart().getX() >= 0) {
				pushOnStack(new CommandAddShape(model, new Line(oldLine.getStart(), temp, strokeColor)));
				resetOldLine();
			} else
				oldLine.setStart(temp);
			break;
		}
		case "square": {
			SquareAddDialog dialog = new SquareAddDialog(temp, strokeColor, fillColor);
			dialog.setModal(true);
			dialog.setVisible(true);
			if (dialog.getData().getLength() > 0) {
				pushOnStack(new CommandAddShape(model, dialog.getData()));
			}
			break;
		}
		case "rectangle": {
			RectangleAddDialog dialog = new RectangleAddDialog(temp, strokeColor, fillColor);
			dialog.setModal(true);
			dialog.setVisible(true);
			if (dialog.getData().getLength() > 0 && dialog.getData().getWidth() > 0) {
				pushOnStack(new CommandAddShape(model, dialog.getData()));
			}
			break;
		}
		case "circle": {
			CircleAddDialog dialog = new CircleAddDialog(temp, strokeColor, fillColor);
			dialog.setModal(true);
			dialog.setVisible(true);
			if (dialog.getData().getRadius() > 0) {
				pushOnStack(new CommandAddShape(model, dialog.getData()));
			}
			break;
		}
		case "triangle": {
			if (oldLine.getStart().getX() >= 0) {
				if (oldLine.getEnd().getX() >= 0) {
					pushOnStack(new CommandAddShape(model,
							new Triangle(oldLine.getStart(), oldLine.getEnd(), temp, strokeColor, fillColor)));
					resetOldLine();
				} else {
					oldLine.setEnd(temp);
				}
			} else {
				oldLine.setStart(temp);
			}
			break;
		}
		case "hexagon": {
			HexagonAddDialog dialog = new HexagonAddDialog(temp, strokeColor, fillColor);
			dialog.setModal(true);
			dialog.setVisible(true);
			if (dialog.getR() > 0) {
				pushOnStack(new CommandAddShape(model, dialog.getData()));
			}
			break;
		}
		}
	}

	private void shouldSelectShape(Point pc) {
		try {
			int i;
			for (i = model.getAll().size() - 1; i >= 0; i--)
				if (model.get(i).contains(pc)) {
					if (model.get(i).isSelected()) {
						pushOnStack(new CommandUnselectShape(model, model.get(i)));
						break;
					}
					pushOnStack(new CommandSelectShape(model, model.get(i)));
					break;
				}
			if (i == -1)
				for (i = 0; i < model.getAll().size(); i++)
					if (model.get(i).isSelected())
						pushOnStack(new CommandUnselectShape(model, model.get(i)));

			setMode("select");
		} catch (Exception IndexOutOfBounds) {

		}
	}

	public void deleteSelected() {
		int dialogButton = JOptionPane.YES_NO_OPTION;
		int dialogResult = JOptionPane.showConfirmDialog(null, "Would You like to delete all selected object?",
				"Warning", dialogButton);
		if (dialogResult == JOptionPane.YES_OPTION) {
			for (int i = 0; i < model.getAll().size(); i++) {
				if (model.get(i).isSelected()) {
					pushOnStack(new CommandRemoveShape(model, model.get(i)));
					i = -1;
				}
			}
		}

	}

	public void modifySelected() {
		Shapes selectedShape = model.getSelectedShape();
		DialogForm dialog;
		if (selectedShape instanceof Point) {
			dialog = new PointModifyDialog((Point) selectedShape.clone());
		} else if (selectedShape instanceof Line) {
			dialog = new LineModifyDialog((Line) selectedShape.clone());
		} else if (selectedShape instanceof Rectangle) {
			dialog = new RectangleModifyDialog((Rectangle) selectedShape.clone());
		} else if (selectedShape instanceof Square) {
			dialog = new SquareModifyDialog((Square) selectedShape.clone());
		} else if (selectedShape instanceof Circle) {
			dialog = new CircleModifyDialog((Circle) selectedShape.clone());
		} else if (selectedShape instanceof Triangle) {
			dialog = new TriangleModifyDialog((Triangle) selectedShape.clone());
		} else {
			dialog = new HexagonModifyDialog((HexagonAdapter) selectedShape.clone());
		}
		dialog.setModal(true);
		dialog.setVisible(true);
		pushOnStack(new CommandModifyShape(selectedShape, dialog.getData()));
	}

	public void pushOnStack(Command command) {
		command.execute();
		model.getLogs().add(command.toString());
		undoStack.push(command);
		redoStack.clear();
		model.notifyObservers(mode, getStackSize(), fromLogs.size());
	}

	public void handleUndo() {
		Command temp = undoStack.pop();
		model.getLogs().add("Undo " + temp.toString());
		temp.unexecute();
		redoStack.push(temp);
		model.notifyObservers(mode, getStackSize(), fromLogs.size());
		if (temp instanceof CommandRemoveShape && undoStack.peek() instanceof CommandRemoveShape) {
			handleUndo();
		}
	}

	public void handleRedo() {
		Command temp = redoStack.pop();
		model.getLogs().add("Redo " + temp.toString());
		temp.execute();
		undoStack.push(temp);
		model.notifyObservers(mode, getStackSize(), fromLogs.size());
		if (!redoStack.isEmpty())
			if (temp instanceof CommandRemoveShape && redoStack.peek() instanceof CommandRemoveShape) {
				handleRedo();
			}
	}

	public void handleStrokeColor() {
		strokeColor = (JColorChooser.showDialog(null, "Stroke color", strokeColor));
		if (strokeColor != null) {
			frame.getBtnStrokeColor().setBackground(strokeColor);
		}
	}

	public void handleFillColor() {
		fillColor = (JColorChooser.showDialog(null, "Fill color", fillColor));
		if (fillColor != null) {
			frame.getBtnFillColor().setBackground(fillColor);
		}
	}

	public void handleToBack() {
		pushOnStack(new CommandToBack(model, model.getSelectedShape()));
	}

	public void handleToFront() {
		pushOnStack(new CommandToFront(model, model.getSelectedShape()));
	}

	public void handleBringToBack() {
		pushOnStack(new CommandBringToBack(model, model.getSelectedShape()));
	}

	public void handleBringToFront() {
		pushOnStack(new CommandBringToFront(model, model.getSelectedShape()));
	}

	public void handleNewFile() {
		resetAll();
		frame.getBtnStrokeColor().setBackground(strokeColor);
		frame.getBtnFillColor().setBackground(fillColor);
	}

	public void handleSaveLog() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter("Txt file", "txt", "text"));
		if (fileChooser.showSaveDialog(frame) == JFileChooser.APPROVE_OPTION) {
			SaveFileManager saveManager = new SaveFileManager(new SaveViaLog(model));
			saveManager.saveFile(Paths.get(fileChooser.getSelectedFile().getAbsolutePath() + ".txt"));
			JOptionPane.showMessageDialog(null,
					"Congratulation You successfully saved logs in " + fileChooser.getSelectedFile().getName(),
					"File report", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public void handleSaveImage() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter("Image file", "png"));
		if (fileChooser.showSaveDialog(frame) == JFileChooser.APPROVE_OPTION) {
			SaveFileManager saveManager = new SaveFileManager(new SaveViaImage(frame));
			saveManager.saveFile(Paths.get(fileChooser.getSelectedFile().getAbsolutePath() + ".png"));
			JOptionPane.showMessageDialog(null,
					"Congratulation You successfully saved drawing in " + fileChooser.getSelectedFile().getName(),
					"File report", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public void handleOpenLog() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter("Txt file", "txt", "text"));
		if (fileChooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
			resetAll();
			OpenFileManager openManager = new OpenFileManager(new OpenViaLog(fromLogs));
			openManager.openFile(Paths.get(fileChooser.getSelectedFile().getAbsolutePath()));

		}
		model.notifyObservers(mode, getStackSize(), fromLogs.size());

	}

	public void handleOpenImage() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter("Image file", "png"));
		if (fileChooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
			resetAll();
			OpenFileManager openManager = new OpenFileManager(new OpenViaImage(frame));
			openManager.openFile(Paths.get(fileChooser.getSelectedFile().getAbsolutePath()));
			JOptionPane.showMessageDialog(null, "Congratulation You successfully opened a drawing", "File report",
					JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public void handleNextLine() {

		String[] commandType = null;
		commandType = fromLogs.remove().split(" - ");
		String[] command = commandType[1].split(" ");
		if (commandType[0].equals("Added")) {
			pushOnStack(new CommandAddShape(model, convertStringToShape(command)));
		} else if (commandType[0].equals("Removed")) {
			pushOnStack(new CommandRemoveShape(model, convertStringToShape(command)));
		} else if (commandType[0].equals("Modified")) {
			String[] modify = commandType[1].split(" into ");
			String[] command1 = modify[0].split(" ");
			command = modify[1].split(" ");
			pushOnStack(new CommandModifyShape(model.get(model.getIndex(convertStringToShape(command1))),
					convertStringToShape(command)));
		} else if (commandType[0].equals("Selected")) {
			pushOnStack(new CommandSelectShape(model, convertStringToShape(command)));
		} else if (commandType[0].equals("Unselected")) {
			pushOnStack(new CommandUnselectShape(model, convertStringToShape(command)));
		} else if (commandType[0].equals("ToFront")) {
			pushOnStack(new CommandToFront(model, convertStringToShape(command)));
		} else if (commandType[0].equals("ToBack")) {
			pushOnStack(new CommandToBack(model, convertStringToShape(command)));
		} else if (commandType[0].equals("BringToBack")) {
			pushOnStack(new CommandBringToBack(model, convertStringToShape(command)));
		} else if (commandType[0].equals("BringToFront")) {
			pushOnStack(new CommandBringToFront(model, convertStringToShape(command)));
		} else {
			command = commandType[0].split(" ");
			if (command[0].equals("Undo")) {
				handleUndo();
			} else if (command[0].equals("Redo")) {
				handleRedo();
			}
		}

	}

	public Shapes convertStringToShape(String[] command) {
		String[] xy = null;
		if (command[0].equals("Point")) {
			xy = command[1].replaceAll("\\(|\\)", "").split(",");
			return new Point(Integer.parseInt(xy[0]), Integer.parseInt(xy[1]), Color.decode(command[3]));
		} else if (command[0].equals("Line")) {
			xy = command[1].replaceAll("\\(|\\)", "").split(",");
			String[] xy1 = command[2].replaceAll("\\(|\\)", "").split(",");
			return new Line(new Point(Integer.parseInt(xy[0]), Integer.parseInt(xy[1])),
					new Point(Integer.parseInt(xy1[0]), Integer.parseInt(xy1[1])), Color.decode(command[4]));
		} else if (command[0].equals("Square")) {
			xy = command[1].replaceAll("\\(|\\)", "").split(",");
			return new Square(new Point(Integer.parseInt(xy[0]), Integer.parseInt(xy[1])),
					Integer.parseInt(command[2].substring(7)), Color.decode(command[4]), Color.decode(command[6]));
		} else if (command[0].equals("Rectangle")) {
			xy = command[1].replaceAll("\\(|\\)", "").split(",");
			return new Rectangle(new Point(Integer.parseInt(xy[0]), Integer.parseInt(xy[1])),
					Integer.parseInt(command[2].substring(7)), Integer.parseInt(command[3].substring(6)),
					Color.decode(command[5]), Color.decode(command[7]));
		} else if (command[0].equals("Circle")) {
			xy = command[1].replaceAll("\\(|\\)", "").split(",");
			return new Circle(new Point(Integer.parseInt(xy[0]), Integer.parseInt(xy[1])),
					Integer.parseInt(command[2].substring(2)), Color.decode(command[4]), Color.decode(command[6]));
		} else if (command[0].equals("Triangle")) {
			xy = command[1].substring(1).replaceAll("\\(|\\)", "").split(",");
			String[] xy1 = command[2].substring(1).replaceAll("\\(|\\)", "").split(",");
			String[] xy2 = command[3].substring(1).replaceAll("\\(|\\)", "").split(",");
			return new Triangle(new Point(Integer.parseInt(xy[0]), Integer.parseInt(xy[1])),
					new Point(Integer.parseInt(xy1[0]), Integer.parseInt(xy1[1])),
					new Point(Integer.parseInt(xy2[0]), Integer.parseInt(xy2[1])), Color.decode(command[5]),
					Color.decode(command[7]));
		} else if (command[0].equals("Hexagon")) {
			xy = command[1].replaceAll("\\(|\\)", "").split(",");
			return new HexagonAdapter(
					new Hexagon(Integer.parseInt(xy[0]), Integer.parseInt(xy[1]),
							Integer.parseInt(command[2].substring(2))),
					Color.decode(command[4]), Color.decode(command[6]));
		}
		return null;
	}

	public void resetAll() {
		oldLine = new Line(new Point(-1, -1), new Point(-1, -1));
		strokeColor = Color.BLACK;
		fillColor = Color.WHITE;
		undoStack.removeAllElements();
		redoStack.removeAllElements();
		fromLogs.clear();
		setMode("point");
		model.resetAll();
	}

	public void handlePoint() {
		setMode("point");
	}

	public void handleLine() {
		setMode("line");
	}

	public void handleSquare() {
		setMode("square");
	}

	public void handleRectangle() {
		setMode("rectangle");
	}

	public void handleCircle() {
		setMode("circle");
	}

	public void handleTriangle() {
		setMode("triangle");
	}

	public void handleHexagon() {
		setMode("hexagon");
	}

	public void handleSelect() {
		setMode("select");
	}

	public void handleModify() {
		setMode("modify");
		modifySelected();
	}

	public void handleDelete() {
		setMode("delete");
		deleteSelected();
	}

}
