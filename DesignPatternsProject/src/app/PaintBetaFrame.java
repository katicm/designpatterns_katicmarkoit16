package app;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;

public class PaintBetaFrame extends JFrame {

	private PaintBetaView view = new PaintBetaView();
	private PaintBetaController controller;

	private JButton btnPoint;
	private JButton btnLine;
	private JButton btnSquare;
	private JButton btnRectangle;
	private JButton btnCircle;
	private JButton btnTriangle;
	private JButton btnHexagon;
	private JScrollPane scrollPane;
	private JButton btnNewFile;
	private JButton btnSavePng;
	private JButton btnOpenPng;
	private JButton btnOpenLog;
	private JButton btnSaveLog;
	private JButton btnNextLine;
	private JButton btnUndo;
	private JButton btnRedo;
	private JButton btnSelect;
	private JButton btnModify;
	private JButton btnDelete;
	private JButton btnFillColor;
	private JButton btnStrokeColor;
	private JButton btnBringToBack;
	private JButton btnToFront;
	private JButton btnBringToFront;
	private JButton btnToBack;
	private JLabel lblMode;
	private JTextArea txtArea;
	private JPanel panel;

	public PaintBetaView getView() {
		return view;
	}

	public void setController(PaintBetaController controller) {
		this.controller = controller;
	}

	public PaintBetaFrame() {
		initComponents();
		createEventHandlers();

	}

	private void initComponents() {

		btnPoint = new JButton("Point");
		btnLine = new JButton("Line");
		btnSquare = new JButton("Square");
		btnRectangle = new JButton("Rectangle");
		btnCircle = new JButton("Circle");
		btnTriangle = new JButton("Triangle");
		panel = new JPanel();
		scrollPane = new JScrollPane();
		btnHexagon = new JButton("Hexagon");

		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup()
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
						.createSequentialGroup().addGap(6)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(btnPoint, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnRectangle, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnCircle, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnLine, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnSquare, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnTriangle, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(btnHexagon,
								GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)))
				.addPreferredGap(ComponentPlacement.RELATED)
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 773, GroupLayout.PREFERRED_SIZE)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 776, GroupLayout.PREFERRED_SIZE))
				.addContainerGap(21, Short.MAX_VALUE)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup()
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
								.addComponent(btnPoint, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(btnLine, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(btnSquare, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(btnRectangle, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(btnCircle, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(btnTriangle, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(btnHexagon, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
								.addComponent(panel, GroupLayout.PREFERRED_SIZE, 474, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 82, Short.MAX_VALUE)))
				.addContainerGap()));

		txtArea = new JTextArea();
		scrollPane.setViewportView(txtArea);
		view.setBackground(Color.WHITE);

		btnNewFile = new JButton("New File");
		btnSavePng = new JButton("Save png");
		btnOpenPng = new JButton("Open png");
		btnOpenLog = new JButton("Open log");
		btnSaveLog = new JButton("Save log");

		btnSelect = new JButton("Select");

		btnNextLine = new JButton("Next line");
		btnNextLine.setEnabled(false);

		btnUndo = new JButton("Undo");
		btnUndo.setEnabled(false);

		btnRedo = new JButton("Redo");
		btnRedo.setEnabled(false);

		btnModify = new JButton("Modify");
		btnModify.setEnabled(false);

		btnDelete = new JButton("Delete");
		btnDelete.setEnabled(false);

		btnFillColor = new JButton("Fill color");
		btnFillColor.setBackground(Color.WHITE);
		btnFillColor.setForeground(Color.BLACK);

		btnStrokeColor = new JButton("Stroke color");
		btnStrokeColor.setBackground(Color.BLACK);
		btnStrokeColor.setForeground(Color.WHITE);

		btnToFront = new JButton("ToFront");
		btnToFront.setEnabled(false);

		btnToBack = new JButton("ToBack");
		btnToBack.setEnabled(false);

		btnBringToBack = new JButton("BringToBack");
		btnBringToBack.setEnabled(false);

		btnBringToFront = new JButton("BringToFront");
		btnBringToFront.setEnabled(false);

		lblMode = new JLabel("Mode : Point");
		lblMode.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
		lblMode.setBackground(Color.RED);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup().addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
								.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
										.addComponent(btnOpenPng, Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
												GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(btnSavePng, Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
												GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(btnNewFile, Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
												GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addGap(18)
								.addGroup(gl_panel
										.createParallelGroup(Alignment.LEADING, false)
										.addComponent(btnOpenLog, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addComponent(btnSaveLog, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addComponent(btnNextLine, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE))
								.addGap(18)
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
										.addGroup(gl_panel.createSequentialGroup().addComponent(btnUndo)
												.addPreferredGap(ComponentPlacement.UNRELATED).addComponent(btnSelect,
														GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE))
										.addGroup(gl_panel.createSequentialGroup().addComponent(btnRedo)
												.addPreferredGap(ComponentPlacement.UNRELATED)
												.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
														.addComponent(btnModify, GroupLayout.DEFAULT_SIZE,
																GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
														.addComponent(
																btnDelete, GroupLayout.DEFAULT_SIZE,
																GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel
										.createSequentialGroup()
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
												.addComponent(btnFillColor, GroupLayout.DEFAULT_SIZE,
														GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(btnStrokeColor, GroupLayout.DEFAULT_SIZE,
														GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_panel.createSequentialGroup().addComponent(btnToBack)
														.addPreferredGap(ComponentPlacement.UNRELATED)
														.addComponent(btnBringToFront))
												.addGroup(gl_panel.createSequentialGroup().addComponent(btnToFront)
														.addPreferredGap(ComponentPlacement.UNRELATED)
														.addComponent(btnBringToBack))))
										.addGroup(gl_panel.createSequentialGroup().addGap(6).addComponent(lblMode))))
						.addComponent(view, GroupLayout.PREFERRED_SIZE, 789, GroupLayout.PREFERRED_SIZE))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		gl_panel.setVerticalGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel
				.createSequentialGroup()
				.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(btnNewFile)
						.addComponent(btnOpenLog).addComponent(btnUndo).addComponent(btnSelect)
						.addComponent(btnFillColor).addComponent(btnToFront).addComponent(btnBringToBack))
				.addPreferredGap(ComponentPlacement.RELATED)
				.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
								.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(btnSavePng)
										.addComponent(btnSaveLog).addComponent(btnRedo).addComponent(btnModify))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
												.addComponent(btnOpenPng).addComponent(btnNextLine))
										.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
												.addComponent(btnDelete).addComponent(lblMode))))
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(btnStrokeColor)
								.addComponent(btnToBack).addComponent(btnBringToFront)))
				.addPreferredGap(ComponentPlacement.RELATED)
				.addComponent(view, GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE)));
		panel.setLayout(gl_panel);
		getContentPane().setLayout(groupLayout);
	}

	private void createEventHandlers() {
		view.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				controller.mouseClicked(arg0);
			}
		});
		btnPoint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handlePoint();
			}
		});
		btnLine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleLine();
			}
		});
		btnSquare.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleSquare();
			}
		});
		btnRectangle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleRectangle();
			}
		});
		btnCircle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleCircle();
			}
		});
		btnTriangle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleTriangle();
			}
		});
		btnHexagon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleHexagon();
			}
		});
		btnStrokeColor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleStrokeColor();
			}
		});
		btnFillColor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleFillColor();
			}
		});
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleSelect();
			}
		});
		btnModify.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleModify();
			}
		});
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleDelete();
			}
		});
		btnToFront.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleToFront();
			}
		});
		btnToBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleToBack();
			}
		});
		btnBringToBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleBringToBack();
			}
		});
		btnBringToFront.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleBringToFront();
			}
		});
		btnUndo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleUndo();
			}
		});
		btnRedo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleRedo();
			}
		});
		btnNewFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleNewFile();
			}
		});
		btnSaveLog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleSaveLog();
			}
		});
		btnSavePng.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleSaveImage();
			}
		});
		btnOpenPng.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleOpenImage();
			}
		});
		btnOpenLog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleOpenLog();
			}
		});
		btnNextLine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.handleNextLine();
			}
		});
	}

	public JButton getBtnStrokeColor() {
		return btnStrokeColor;
	}

	public JButton getBtnFillColor() {
		return btnFillColor;
	}

	public JLabel getLblMode() {
		return lblMode;
	}

	public JButton getBtnDelete() {
		return btnDelete;
	}

	public JButton getBtnModify() {
		return btnModify;
	}

	public JButton getBtnToBack() {
		return btnToBack;
	}

	public JButton getBtnToFront() {
		return btnToFront;
	}

	public JButton getBtnBringToBack() {
		return btnBringToBack;
	}

	public JButton getBtnBringToFront() {
		return btnBringToFront;
	}

	public JButton getBtnUndo() {
		return btnUndo;
	}

	public JButton getBtnRedo() {
		return btnRedo;
	}

	public JButton getNextLine() {
		return btnNextLine;
	}

	public JTextArea getTxtArea() {
		return txtArea;
	}

}
