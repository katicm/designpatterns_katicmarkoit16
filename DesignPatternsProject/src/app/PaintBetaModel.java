package app;

import java.util.ArrayList;
import java.util.Iterator;

import observer.Observer;
import observer.iObservable;
import shapes.Shapes;

public class PaintBetaModel implements iObservable {
	private ArrayList<Shapes> shapesList = new ArrayList<Shapes>();
	private ArrayList<Observer> observers;
	private ArrayList<String> logs = new ArrayList<String>();

	public PaintBetaModel() {
		observers = new ArrayList<Observer>();
	}

	public void add(Shapes p) {
		shapesList.add(p);
	}

	public Shapes get(int i) {
		return shapesList.get(i);
	}

	public ArrayList<Shapes> getAll() {
		return shapesList;
	}

	public ArrayList<String> getLogs() {
		return logs;
	}

	public int getIndex(Shapes shape) {
		for (int i = 0; i < shapesList.size(); i++) {
			if (shapesList.get(i).equals(shape)) {
				return i;
			}
		}
		return -1;
	}

	public void remove(Shapes p) {
		shapesList.remove(p);
	}

	/// iOBSERVABLE ///
	@Override
	public void addObserver(Observer observer) {
		observers.add(observer);
	}

	@Override
	public void removeObserver(Observer observer) {
		observers.remove(observer);
	}

	@Override
	public void notifyObservers(String mode, int[] sizeStack, int sizeFLog) {
		Iterator<Observer> it = observers.iterator();
		while (it.hasNext()) {
			it.next().update(countSelectedShapes(), mode, sizeStack, logs, sizeFLog);
		}
	}

	public int countSelectedShapes() {
		int count = 0;
		for (Shapes s : shapesList) {
			if (s.isSelected())
				count++;
		}
		return count;
	}

	public Shapes getSelectedShape() {
		for (Shapes s : shapesList) {
			if (s.isSelected())
				return s;
		}
		return null;
	}

	public void resetAll() {
		shapesList.clear();
		logs.clear();
	}

}
