package adapter;

import java.awt.Color;
import java.awt.Graphics;

import hexagon.Hexagon;
import shapes.Paintable;
import shapes.Point;

public class HexagonAdapter extends Paintable {
	private Hexagon hexagon;

	public HexagonAdapter(Hexagon hexagon) {
		this.hexagon = hexagon;
	}

	public HexagonAdapter(Hexagon hexagon, Color color, Color fillColor) {
		this.hexagon = hexagon;
		hexagon.setBorderColor(color);
		hexagon.setAreaColor(fillColor);
	}

	public void setCenter(Point center) {
		hexagon.setX(center.getX());
		hexagon.setY(center.getY());
	}

	public Point getCenter() {
		return new Point(hexagon.getX(), hexagon.getY());
	}

	public void setR(int r) {
		hexagon.setR(r);
	}

	public int getR() {
		return hexagon.getR();
	}

	public void setColor(Color color) {
		hexagon.setBorderColor(color);
	}

	public Color getColor() {
		return hexagon.getBorderColor();
	}

	public void setFillColor(Color fillColor) {
		hexagon.setAreaColor(fillColor);
	}

	public Color getFillColor() {
		return hexagon.getAreaColor();
	}

	@Override
	public double surface() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double perimeter() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void fill(Graphics gc) {
		gc.setColor(hexagon.getAreaColor());
		gc.fillRect(hexagon.getX() + 1, hexagon.getY() + 1, hexagon.getR() - 1, hexagon.getR() - 1);

	}

	@Override
	public void paint(Graphics gc) {
		gc.setColor(hexagon.getBorderColor());
		hexagon.paint(gc);
	}

	@Override
	public void select(Graphics gc) {

	}

	@Override
	public boolean contains(Point pc) {
		return hexagon.doesContain(pc.getX(), pc.getY());
	}

	@Override
	public void moveTo(int x, int y) {
		hexagon.setX(x);
		hexagon.setY(y);
	}

	@Override
	public void moveFor(int x, int y) {
		hexagon.setX(hexagon.getX() + x);
		hexagon.setY(hexagon.getY() + y);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof HexagonAdapter) {
			HexagonAdapter help = (HexagonAdapter) obj;
			if (this.hexagon.getX() == help.hexagon.getX() && this.hexagon.getY() == help.hexagon.getY()
					&& this.hexagon.getR() == help.hexagon.getR())
				return true;
			else
				return false;
		} else
			return false;
	}

	public String toString() {
		return "Hexagon (" + hexagon.getX() + "," + hexagon.getY() + ") r=" + hexagon.getR() + " StrokeColor "
				+ getHexColor(getColor()) + " FillColor " + getHexColor(getFillColor());
	}

	public void setSelected(boolean selected) {
		hexagon.setSelected(selected);
	}

	public boolean isSelected() {
		return hexagon.isSelected();
	}

	public HexagonAdapter clone() {
		return new HexagonAdapter(new Hexagon(hexagon.getX(), hexagon.getY(), hexagon.getR()), this.getColor(),
				this.getFillColor());
	}

}
