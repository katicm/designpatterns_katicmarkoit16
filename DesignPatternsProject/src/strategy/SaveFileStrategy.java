package strategy;

import java.nio.file.Path;

public interface SaveFileStrategy {
	public void saveFile(Path path);
}
