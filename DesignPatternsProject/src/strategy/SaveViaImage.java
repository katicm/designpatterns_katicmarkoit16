package strategy;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import javax.imageio.ImageIO;

import app.PaintBetaFrame;

public class SaveViaImage implements SaveFileStrategy {
	private PaintBetaFrame frame;

	public SaveViaImage(PaintBetaFrame frame) {
		this.frame = frame;
	}

	@Override
	public void saveFile(Path path) {
		try {
			BufferedImage buff = new BufferedImage(frame.getView().getWidth(), frame.getView().getHeight(),
					BufferedImage.TYPE_INT_ARGB);
			frame.getView().paint(buff.getGraphics());
			ImageIO.write(buff, "PNG", new File(path.toString()));
		} catch (IOException err) {

		}
	}
}
