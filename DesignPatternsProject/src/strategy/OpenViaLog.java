package strategy;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Queue;

public class OpenViaLog implements OpenFileStrategy {
	private Queue<String> fromLogs;

	public OpenViaLog(Queue<String> fromLogs) {
		this.fromLogs = fromLogs;
	}

	BufferedReader buff = null;

	@Override
	public void openFile(Path path) {
		try {
			buff = new BufferedReader(new FileReader(path.toString()));
			String line = null;
			while ((line = buff.readLine()) != null) {
				fromLogs.add(line);
			}
		} catch (FileNotFoundException err) {

		} catch (IOException err) {

		}

	}

}
