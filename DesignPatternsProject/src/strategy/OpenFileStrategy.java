package strategy;

import java.nio.file.Path;

public interface OpenFileStrategy {

	public void openFile(Path path);
}
