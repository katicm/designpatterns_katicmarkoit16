package strategy;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

import app.PaintBetaModel;

public class SaveViaLog implements SaveFileStrategy {
	private PaintBetaModel model;

	public SaveViaLog(PaintBetaModel model) {
		this.model = model;
	}

	@Override
	public void saveFile(Path path) {
		try {
			Files.write(path, model.getLogs(), Charset.defaultCharset());
		} catch (IOException err) {

		}
	}
}
