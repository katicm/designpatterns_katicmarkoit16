package strategy;

import java.nio.file.Path;

public class OpenFileManager implements OpenFileStrategy {
	private OpenFileStrategy openManager;

	public OpenFileManager(OpenFileStrategy openManager) {
		this.openManager = openManager;
	}

	@Override
	public void openFile(Path path) {
		openManager.openFile(path);

	}

}
