package strategy;

import java.nio.file.Path;

public class SaveFileManager implements SaveFileStrategy {
	private SaveFileStrategy saveManager;

	public SaveFileManager(SaveFileStrategy saveManager) {
		this.saveManager = saveManager;
	}

	@Override
	public void saveFile(Path path) {
		saveManager.saveFile(path);
	}

}
