package strategy;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import app.PaintBetaFrame;

public class OpenViaImage implements OpenFileStrategy {
	private PaintBetaFrame frame;

	public OpenViaImage(PaintBetaFrame frame) {
		this.frame = frame;
	}

	@Override
	public void openFile(Path path) {
		try {
			BufferedImage buff = ImageIO.read(path.toFile());
			frame.getView().removeAll();
			frame.getView().add(new JLabel(new ImageIcon(buff)));
			frame.setVisible(true);
		} catch (IOException err) {

		}
	}

}
