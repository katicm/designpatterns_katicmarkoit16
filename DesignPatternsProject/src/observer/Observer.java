package observer;

import java.util.ArrayList;

public interface Observer {
	void update(int numOfSelShapes, String mode, int[] stackSize, ArrayList<String> logs, int sizeFLog);
}
