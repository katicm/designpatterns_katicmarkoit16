package observer;

import java.util.ArrayList;

import app.PaintBetaFrame;

public class ObserverButton implements Observer {

	private PaintBetaFrame frame;

	public ObserverButton(PaintBetaFrame frame) {
		this.frame = frame;
	}

	@Override
	public void update(int numOfSelShapes, String mode, int[] stackSize, ArrayList<String> logs, int sizeFLog) {
		if (numOfSelShapes != 0) {
			frame.getBtnDelete().setEnabled(true);
			if (numOfSelShapes == 1) {
				frame.getBtnModify().setEnabled(true);
				frame.getBtnBringToBack().setEnabled(true);
				frame.getBtnBringToFront().setEnabled(true);
				frame.getBtnToBack().setEnabled(true);
				frame.getBtnToFront().setEnabled(true);
			} else {
				frame.getBtnModify().setEnabled(false);
				frame.getBtnBringToBack().setEnabled(false);
				frame.getBtnBringToFront().setEnabled(false);
				frame.getBtnToBack().setEnabled(false);
				frame.getBtnToFront().setEnabled(false);
			}
		} else {
			frame.getBtnDelete().setEnabled(false);
			frame.getBtnModify().setEnabled(false);
			frame.getBtnBringToBack().setEnabled(false);
			frame.getBtnBringToFront().setEnabled(false);
			frame.getBtnToBack().setEnabled(false);
			frame.getBtnToFront().setEnabled(false);
		}
		switch (mode) {
		case "point": {
			frame.getLblMode().setText("Mode : Point");
			break;
		}
		case "line": {
			frame.getLblMode().setText("Mode : Line");
			break;
		}
		case "square": {
			frame.getLblMode().setText("Mode : Square");
			break;
		}
		case "rectangle": {
			frame.getLblMode().setText("Mode : Rectangle");
			break;
		}
		case "circle": {
			frame.getLblMode().setText("Mode : Circle");
			break;
		}
		case "triangle": {
			frame.getLblMode().setText("Mode : Triangle");
			break;
		}
		case "hexagon": {
			frame.getLblMode().setText("Mode : Hexagon");
			break;
		}
		case "select": {
			frame.getLblMode().setText("Mode : Select");
			break;
		}
		case "delete": {
			frame.getLblMode().setText("Mode : Delete");
			break;
		}
		case "modify": {
			frame.getLblMode().setText("Mode : Modify");
			break;
		}
		}
		if (stackSize[0] > 0) {
			frame.getBtnUndo().setEnabled(true);
		} else {
			frame.getBtnUndo().setEnabled(false);
		}
		if (stackSize[1] > 0) {
			frame.getBtnRedo().setEnabled(true);
		} else {
			frame.getBtnRedo().setEnabled(false);
		}
		if (sizeFLog == 0) {
			frame.getNextLine().setEnabled(false);
		} else {
			frame.getNextLine().setEnabled(true);
		}
		frame.getTxtArea().setText("");
		for (String s : logs) {
			frame.getTxtArea().append(s + "\n");
		}
		frame.repaint();
	}

}
