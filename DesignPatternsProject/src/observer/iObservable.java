package observer;

public interface iObservable {
	void addObserver(Observer observer);

	void removeObserver(Observer observer);

	void notifyObservers(String mode, int[] size, int sizeFLog);
}
