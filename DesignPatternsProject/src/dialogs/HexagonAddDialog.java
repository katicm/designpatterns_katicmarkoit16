package dialogs;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import adapter.HexagonAdapter;
import hexagon.Hexagon;
import shapes.Point;

public class HexagonAddDialog extends DialogForm {
	private JTextField txtX;
	private JButton btnOK;
	private JButton btnCancel;
	private JButton btnFill;
	private JButton btnStroke;
	private JTextField txtY;
	private JTextField txtRadius;
	private JLabel lblX;
	private JLabel lblY;
	private JLabel lblStrokeColor;
	private JLabel lblFillColor;
	private JLabel lblRange;
	private Point point;
	private Color stroke;
	private Color fill;
	private HexagonAdapter hexagon;

	public HexagonAddDialog(Point point, Color stroke, Color fill) {
		this.point = point;
		this.stroke = stroke;
		this.fill = fill;
		initComponents();
		createEventHandlers();
	}

	public void initComponents() {
		setBounds(100, 100, 400, 350);
		setTitle("Input R of a hexagon");
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		txtRadius = new JTextField();
		txtRadius.setBounds(170, 79, 78, 30);
		getContentPane().add(txtRadius);

		lblX = new JLabel("Center    X=");
		lblX.setBounds(46, 33, 97, 23);
		getContentPane().add(lblX);

		lblY = new JLabel("Y=");
		lblY.setBounds(232, 33, 30, 23);
		getContentPane().add(lblY);

		lblStrokeColor = new JLabel("Stroke color");
		lblStrokeColor.setBounds(62, 151, 108, 15);
		getContentPane().add(lblStrokeColor);

		lblFillColor = new JLabel("Fill color");
		lblFillColor.setBounds(92, 212, 67, 15);
		getContentPane().add(lblFillColor);

		lblRange = new JLabel("R =");
		lblRange.setBounds(101, 86, 69, 15);
		getContentPane().add(lblRange);

		txtY = new JTextField(Integer.toString(point.getY()));
		txtY.setEditable(false);
		txtY.setBounds(267, 30, 78, 30);
		getContentPane().add(txtY);
		txtY.setColumns(10);

		txtX = new JTextField(Integer.toString(point.getX()));
		txtX.setEditable(false);
		txtX.setBounds(141, 30, 78, 30);
		getContentPane().add(txtX);

		btnOK = new JButton("Confirm");
		Border borderOK = new LineBorder(Color.GREEN, 5);
		btnOK.setBorder(borderOK);
		btnOK.setBackground(Color.WHITE);
		btnOK.setBounds(92, 265, 97, 36);
		getContentPane().add(btnOK);

		btnCancel = new JButton("Cancel");
		Border borderCancel = new LineBorder(Color.RED, 5);
		btnCancel.setBorder(borderCancel);
		btnCancel.setBackground(Color.WHITE);
		btnCancel.setBounds(201, 265, 110, 36);
		getContentPane().add(btnCancel);

		btnFill = new JButton("");
		btnFill.setBackground(fill);
		btnFill.setBounds(170, 197, 175, 30);
		getContentPane().add(btnFill);

		btnStroke = new JButton("");
		btnStroke.setBackground(stroke);
		btnStroke.setBounds(170, 136, 175, 30);
		getContentPane().add(btnStroke);
	}

	public void createEventHandlers() {
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					hexagon = new HexagonAdapter(
							new Hexagon(point.getX(), point.getY(), Integer.parseInt(txtRadius.getText())),
							btnStroke.getBackground(), btnFill.getBackground());

				} catch (NumberFormatException nfe) {
				}
				dispose();
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnStroke.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStroke.setBackground(JColorChooser.showDialog(null, "Choose new Stroke color", null));
			}
		});
		btnFill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnFill.setBackground(JColorChooser.showDialog(null, "Choose new Fill color", null));
			}
		});
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				txtRadius.requestFocus();
			}
		});
	}

	public HexagonAdapter getData() {
		return hexagon;
	}

	public int getR() {
		try {
			return Integer.parseInt(txtRadius.getText());
		} catch (NumberFormatException e) {
			return 0;
		}
	}
}
