package dialogs;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import shapes.Point;

public class PointModifyDialog extends DialogForm {
	private JTextField txtX;
	private JButton btnOK;
	private JButton btnCancel;
	private JButton btnStroke;
	private JTextField txtY;
	private JLabel lblX;
	private JLabel lblY;
	private JLabel lblStrokeColor;
	private Point point;

	public PointModifyDialog(Point point) {
		this.point = point;
		initComponents();
		createEventHandlers();
	}

	public void initComponents() {
		setBounds(100, 100, 400, 250);
		setTitle("Input radius of a Point");
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		lblX = new JLabel("X=");
		lblX.setBounds(92, 33, 30, 23);
		getContentPane().add(lblX);

		lblY = new JLabel("Y=");
		lblY.setBounds(232, 33, 30, 23);
		getContentPane().add(lblY);

		lblStrokeColor = new JLabel("Stroke color");
		lblStrokeColor.setBounds(62, 102, 108, 15);
		getContentPane().add(lblStrokeColor);

		txtY = new JTextField(Integer.toString(point.getY()));
		txtY.setBounds(267, 30, 78, 30);
		getContentPane().add(txtY);
		txtY.setColumns(10);

		txtX = new JTextField(Integer.toString(point.getX()));
		txtX.setBounds(141, 30, 78, 30);
		getContentPane().add(txtX);

		btnOK = new JButton("Confirm");
		Border borderOK = new LineBorder(Color.GREEN, 5);
		btnOK.setBorder(borderOK);
		btnOK.setBackground(Color.WHITE);
		btnOK.setBounds(92, 150, 97, 36);
		getContentPane().add(btnOK);

		btnCancel = new JButton("Cancel");
		Border borderCancel = new LineBorder(Color.RED, 5);
		btnCancel.setBorder(borderCancel);
		btnCancel.setBackground(Color.WHITE);
		btnCancel.setBounds(201, 150, 110, 36);
		getContentPane().add(btnCancel);

		btnStroke = new JButton("");
		btnStroke.setBackground(point.getColor());
		btnStroke.setBounds(170, 87, 175, 30);
		getContentPane().add(btnStroke);
	}

	public void createEventHandlers() {
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					point.setX(Integer.parseInt(txtX.getText()));
					point.setY(Integer.parseInt(txtY.getText()));
					point.setColor(btnStroke.getBackground());
				} catch (NumberFormatException nfe) {
				}
				dispose();
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnStroke.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStroke.setBackground(JColorChooser.showDialog(null, "Choose new Stroke color", point.getColor()));
			}
		});
	}

	public Point getData() {
		return point;
	}
}
