package dialogs;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import shapes.Point;
import shapes.Triangle;

public class TriangleModifyDialog extends DialogForm {
	private JTextField txtCX;
	private JButton btnOK;
	private JButton btnCancel;
	private JButton btnStroke;
	private JTextField txtCY;
	private JLabel lblX;
	private JLabel lblY;
	private JLabel lblStrokeColor;
	private Triangle triangle;
	private JTextField txtBY;
	private JLabel label;
	private JTextField txtBX;
	private JLabel lblEndX;
	private JTextField txtAY;
	private JTextField txtAX;
	private JLabel lblAX;
	private JLabel label_1;
	private JLabel lblFillColor;
	private JButton btnFill;

	public TriangleModifyDialog(Triangle triangle) {
		this.triangle = triangle;
		initComponents();
		createEventHandlers();
	}

	public void initComponents() {
		setBounds(100, 100, 400, 350);
		setTitle("Input radius of a Line");
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		lblX = new JLabel("C     X=");
		lblX.setBounds(65, 33, 77, 23);
		getContentPane().add(lblX);

		lblY = new JLabel("Y=");
		lblY.setBounds(232, 33, 30, 23);
		getContentPane().add(lblY);

		lblStrokeColor = new JLabel("Stroke color");
		lblStrokeColor.setBounds(62, 172, 108, 15);
		getContentPane().add(lblStrokeColor);

		txtCY = new JTextField(Integer.toString(triangle.getVertexC().getY()));
		txtCY.setBounds(267, 30, 78, 30);
		getContentPane().add(txtCY);
		txtCY.setColumns(10);

		txtCX = new JTextField(Integer.toString(triangle.getVertexC().getX()));
		txtCX.setBounds(141, 30, 78, 30);
		getContentPane().add(txtCX);

		btnOK = new JButton("Confirm");
		Border borderOK = new LineBorder(Color.GREEN, 5);
		btnOK.setBorder(borderOK);
		btnOK.setBackground(Color.WHITE);
		btnOK.setBounds(88, 265, 97, 36);
		getContentPane().add(btnOK);

		btnCancel = new JButton("Cancel");
		Border borderCancel = new LineBorder(Color.RED, 5);
		btnCancel.setBorder(borderCancel);
		btnCancel.setBackground(Color.WHITE);
		btnCancel.setBounds(197, 265, 110, 36);
		getContentPane().add(btnCancel);

		btnStroke = new JButton("");
		btnStroke.setBackground(triangle.getColor());
		btnStroke.setBounds(170, 157, 175, 30);
		getContentPane().add(btnStroke);

		txtBY = new JTextField(Integer.toString(triangle.getVertexB().getY()));
		txtBY.setColumns(10);
		txtBY.setBounds(267, 68, 78, 30);
		getContentPane().add(txtBY);

		label = new JLabel("Y=");
		label.setBounds(232, 71, 30, 23);
		getContentPane().add(label);

		txtBX = new JTextField(Integer.toString(triangle.getVertexB().getX()));
		txtBX.setBounds(141, 68, 78, 30);
		getContentPane().add(txtBX);

		lblEndX = new JLabel("B     X=");
		lblEndX.setBounds(65, 71, 77, 23);
		getContentPane().add(lblEndX);

		btnFill = new JButton("");
		btnFill.setBackground(triangle.getFillColor());
		btnFill.setBounds(170, 199, 175, 30);
		getContentPane().add(btnFill);

		lblFillColor = new JLabel("Fill Color");
		lblFillColor.setBounds(62, 214, 108, 15);
		getContentPane().add(lblFillColor);

		label_1 = new JLabel("Y=");
		label_1.setBounds(232, 109, 30, 23);
		getContentPane().add(label_1);

		txtAY = new JTextField(Integer.toString(triangle.getVertexA().getY()));
		txtAY.setColumns(10);
		txtAY.setBounds(267, 106, 78, 30);
		getContentPane().add(txtAY);

		txtAX = new JTextField(Integer.toString(triangle.getVertexA().getX()));
		txtAX.setBounds(141, 106, 78, 30);
		getContentPane().add(txtAX);

		lblAX = new JLabel("A     X=");
		lblAX.setBounds(65, 109, 77, 23);
		getContentPane().add(lblAX);
	}

	public void createEventHandlers() {
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					triangle.setVertexC(
							new Point(Integer.parseInt(txtCX.getText()), Integer.parseInt(txtCY.getText())));
					triangle.setVertexB(
							new Point(Integer.parseInt(txtBX.getText()), Integer.parseInt(txtBY.getText())));
					triangle.setVertexA(
							new Point(Integer.parseInt(txtAX.getText()), Integer.parseInt(txtAY.getText())));
					triangle.setColor(btnStroke.getBackground());
					triangle.setFillColor(btnFill.getBackground());

				} catch (NumberFormatException nfe) {
				}
				dispose();
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnStroke.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStroke.setBackground(JColorChooser.showDialog(null, "Choose new Stroke color", triangle.getColor()));
			}
		});
		btnFill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnFill.setBackground(JColorChooser.showDialog(null, "Choose new Fill color", triangle.getColor()));
			}
		});
	}

	public Triangle getData() {
		return triangle;
	}
}
