package dialogs;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import shapes.Point;
import shapes.Rectangle;

public class RectangleAddDialog extends DialogForm {
	private JTextField txtX;
	private JButton btnOK;
	private JButton btnCancel;
	private JButton btnFill;
	private JButton btnStroke;
	private JTextField txtY;
	private JTextField txtLength;
	private JLabel lblX;
	private JLabel lblY;
	private JLabel lblStrokeColor;
	private JLabel lblFillColor;
	private JLabel lblRange;
	private Rectangle rectangle;
	private JTextField txtWidth;

	public RectangleAddDialog(Point point, Color stroke, Color fill) {
		rectangle = new Rectangle();
		rectangle.setCorner(point);
		rectangle.setColor(stroke);
		rectangle.setFillColor(fill);
		initComponents();
		createEventHandlers();
	}

	public void initComponents() {
		setBounds(100, 100, 400, 350);
		setTitle("Input radius of a rectangle");
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		txtLength = new JTextField();
		txtLength.setBounds(81, 86, 78, 30);
		getContentPane().add(txtLength);

		lblX = new JLabel("Center    X=");
		lblX.setBounds(46, 33, 97, 23);
		getContentPane().add(lblX);

		lblY = new JLabel("Y=");
		lblY.setBounds(232, 33, 30, 23);
		getContentPane().add(lblY);

		lblStrokeColor = new JLabel("Stroke color");
		lblStrokeColor.setBounds(62, 151, 108, 15);
		getContentPane().add(lblStrokeColor);

		lblFillColor = new JLabel("Fill color");
		lblFillColor.setBounds(92, 212, 67, 15);
		getContentPane().add(lblFillColor);

		lblRange = new JLabel("Length=");
		lblRange.setBounds(12, 93, 69, 15);
		getContentPane().add(lblRange);

		txtY = new JTextField(Integer.toString(rectangle.getCorner().getY()));
		txtY.setEditable(false);
		txtY.setBounds(267, 30, 78, 30);
		getContentPane().add(txtY);
		txtY.setColumns(10);

		txtX = new JTextField(Integer.toString(rectangle.getCorner().getX()));
		txtX.setEditable(false);
		txtX.setBounds(141, 30, 78, 30);
		getContentPane().add(txtX);

		btnOK = new JButton("Confirm");
		Border borderOK = new LineBorder(Color.GREEN, 5);
		btnOK.setBorder(borderOK);
		btnOK.setBackground(Color.WHITE);
		btnOK.setBounds(92, 265, 97, 36);
		getContentPane().add(btnOK);

		btnCancel = new JButton("Cancel");
		Border borderCancel = new LineBorder(Color.RED, 5);
		btnCancel.setBorder(borderCancel);
		btnCancel.setBackground(Color.WHITE);
		btnCancel.setBounds(201, 265, 110, 36);
		getContentPane().add(btnCancel);

		btnFill = new JButton("");
		btnFill.setBackground(rectangle.getFillColor());
		btnFill.setBounds(170, 197, 175, 30);
		getContentPane().add(btnFill);

		btnStroke = new JButton("");
		btnStroke.setBackground(rectangle.getColor());
		btnStroke.setBounds(170, 136, 175, 30);
		getContentPane().add(btnStroke);

		JLabel lblWidth = new JLabel("Width=");
		lblWidth.setBounds(201, 93, 69, 15);
		getContentPane().add(lblWidth);

		txtWidth = new JTextField();
		txtWidth.setBounds(270, 86, 78, 30);
		getContentPane().add(txtWidth);
	}

	public void createEventHandlers() {
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					rectangle.setLength(Integer.parseInt(txtLength.getText()));
					rectangle.setWidth(Integer.parseInt(txtWidth.getText()));
					rectangle.setColor(btnStroke.getBackground());
					rectangle.setFillColor(btnFill.getBackground());
				} catch (NumberFormatException nfe) {
				}
				dispose();
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnStroke.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStroke.setBackground(JColorChooser.showDialog(null, "Choose new Stroke color", null));
			}
		});
		btnFill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnFill.setBackground(JColorChooser.showDialog(null, "Choose new Fill color", null));
			}
		});
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				txtLength.requestFocus();
			}
		});
	}

	public Rectangle getData() {
		return rectangle;
	}
}
