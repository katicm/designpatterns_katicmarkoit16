package dialogs;

import javax.swing.JDialog;

import shapes.Shapes;

public abstract class DialogForm extends JDialog {
	public abstract Shapes getData();
}
