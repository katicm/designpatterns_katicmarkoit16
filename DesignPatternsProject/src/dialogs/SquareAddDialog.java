package dialogs;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import shapes.Point;
import shapes.Square;

public class SquareAddDialog extends DialogForm {
	private JTextField txtX;
	private JButton btnOK;
	private JButton btnCancel;
	private JButton btnFill;
	private JButton btnStroke;
	private JTextField txtY;
	private JTextField txtLength;
	private JLabel lblX;
	private JLabel lblY;
	private JLabel lblStrokeColor;
	private JLabel lblFillColor;
	private JLabel lblRange;
	private Square square;

	public SquareAddDialog(Point point, Color stroke, Color fill) {
		square = new Square();
		square.setCorner(point);
		square.setColor(stroke);
		square.setFillColor(fill);
		initComponents();
		createEventHandlers();
	}

	public void initComponents() {
		setBounds(100, 100, 400, 350);
		setTitle("Input radius of a square");
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		txtLength = new JTextField();
		txtLength.setBounds(170, 79, 78, 30);
		getContentPane().add(txtLength);

		lblX = new JLabel("Corner    X=");
		lblX.setBounds(46, 33, 97, 23);
		getContentPane().add(lblX);

		lblY = new JLabel("Y=");
		lblY.setBounds(232, 33, 30, 23);
		getContentPane().add(lblY);

		lblStrokeColor = new JLabel("Stroke color");
		lblStrokeColor.setBounds(62, 151, 108, 15);
		getContentPane().add(lblStrokeColor);

		lblFillColor = new JLabel("Fill color");
		lblFillColor.setBounds(92, 212, 67, 15);
		getContentPane().add(lblFillColor);

		lblRange = new JLabel("Length=");
		lblRange.setBounds(101, 86, 69, 15);
		getContentPane().add(lblRange);

		txtY = new JTextField(Integer.toString(square.getCorner().getY()));
		txtY.setEditable(false);
		txtY.setBounds(267, 30, 78, 30);
		getContentPane().add(txtY);
		txtY.setColumns(10);

		txtX = new JTextField(Integer.toString(square.getCorner().getX()));
		txtX.setEditable(false);
		txtX.setBounds(141, 30, 78, 30);
		getContentPane().add(txtX);

		btnOK = new JButton("Confirm");
		Border borderOK = new LineBorder(Color.GREEN, 5);
		btnOK.setBorder(borderOK);
		btnOK.setBackground(Color.WHITE);
		btnOK.setBounds(92, 265, 97, 36);
		getContentPane().add(btnOK);

		btnCancel = new JButton("Cancel");
		Border borderCancel = new LineBorder(Color.RED, 5);
		btnCancel.setBorder(borderCancel);
		btnCancel.setBackground(Color.WHITE);
		btnCancel.setBounds(201, 265, 110, 36);
		getContentPane().add(btnCancel);

		btnFill = new JButton("");
		btnFill.setBackground(square.getFillColor());
		btnFill.setBounds(170, 197, 175, 30);
		getContentPane().add(btnFill);

		btnStroke = new JButton("");
		btnStroke.setBackground(square.getColor());
		btnStroke.setBounds(170, 136, 175, 30);
		getContentPane().add(btnStroke);
	}

	public void createEventHandlers() {
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					square.setLength(Integer.parseInt(txtLength.getText()));
					square.setColor(btnStroke.getBackground());
					square.setFillColor(btnFill.getBackground());
				} catch (NumberFormatException nfe) {
				}
				dispose();
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnStroke.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStroke.setBackground(JColorChooser.showDialog(null, "Choose new Stroke color", null));
			}
		});
		btnFill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnFill.setBackground(JColorChooser.showDialog(null, "Choose new Fill color", null));
			}
		});
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				txtLength.requestFocus();
			}
		});
	}

	@Override
	public Square getData() {
		return square;
	}

}
