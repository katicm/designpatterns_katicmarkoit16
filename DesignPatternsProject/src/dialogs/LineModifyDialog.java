package dialogs;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import shapes.Line;
import shapes.Point;

public class LineModifyDialog extends DialogForm {
	private JTextField txtStartX;
	private JButton btnOK;
	private JButton btnCancel;
	private JButton btnStroke;
	private JTextField txtStartY;
	private JLabel lblX;
	private JLabel lblY;
	private JLabel lblStrokeColor;
	private Line line;
	private JTextField txtEndY;
	private JLabel lblEndY;
	private JTextField txtEndX;
	private JLabel lblEndX;

	public LineModifyDialog(Line line) {
		this.line = line;
		initComponents();
		createEventHandlers();
	}

	public void initComponents() {
		setBounds(100, 100, 400, 280);
		setTitle("Input radius of a Line");
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		lblX = new JLabel(" Start    X=");
		lblX.setBounds(46, 33, 97, 23);
		getContentPane().add(lblX);

		lblY = new JLabel("Y=");
		lblY.setBounds(232, 33, 30, 23);
		getContentPane().add(lblY);

		lblStrokeColor = new JLabel("Stroke color");
		lblStrokeColor.setBounds(62, 151, 108, 15);
		getContentPane().add(lblStrokeColor);

		txtStartY = new JTextField(Integer.toString(line.getStart().getY()));
		txtStartY.setBounds(267, 30, 78, 30);
		getContentPane().add(txtStartY);
		txtStartY.setColumns(10);

		txtStartX = new JTextField(Integer.toString(line.getStart().getX()));
		txtStartX.setBounds(141, 30, 78, 30);
		getContentPane().add(txtStartX);

		btnOK = new JButton("Confirm");
		Border borderOK = new LineBorder(Color.GREEN, 5);
		btnOK.setBorder(borderOK);
		btnOK.setBackground(Color.WHITE);
		btnOK.setBounds(96, 203, 97, 36);
		getContentPane().add(btnOK);

		btnCancel = new JButton("Cancel");
		Border borderCancel = new LineBorder(Color.RED, 5);
		btnCancel.setBorder(borderCancel);
		btnCancel.setBackground(Color.WHITE);
		btnCancel.setBounds(205, 203, 110, 36);
		getContentPane().add(btnCancel);

		btnStroke = new JButton("");
		btnStroke.setBackground(line.getColor());
		btnStroke.setBounds(170, 136, 175, 30);
		getContentPane().add(btnStroke);

		txtEndY = new JTextField(Integer.toString(line.getEnd().getY()));
		txtEndY.setColumns(10);
		txtEndY.setBounds(267, 77, 78, 30);
		getContentPane().add(txtEndY);

		lblEndY = new JLabel("Y=");
		lblEndY.setBounds(232, 80, 30, 23);
		getContentPane().add(lblEndY);

		txtEndX = new JTextField(Integer.toString(line.getEnd().getX()));
		txtEndX.setBounds(141, 77, 78, 30);
		getContentPane().add(txtEndX);

		lblEndX = new JLabel("   End    X=");
		lblEndX.setBounds(46, 80, 97, 23);
		getContentPane().add(lblEndX);
	}

	public void createEventHandlers() {
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					line.setStart(
							new Point(Integer.parseInt(txtStartX.getText()), Integer.parseInt(txtStartY.getText())));
					line.setEnd(new Point(Integer.parseInt(txtEndX.getText()), Integer.parseInt(txtEndY.getText())));
					line.setColor(btnStroke.getBackground());
				} catch (NumberFormatException nfe) {
				}
				dispose();
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnStroke.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStroke.setBackground(JColorChooser.showDialog(null, "Choose new Stroke color", line.getColor()));
			}
		});
	}

	public Line getData() {
		return line;
	}
}
