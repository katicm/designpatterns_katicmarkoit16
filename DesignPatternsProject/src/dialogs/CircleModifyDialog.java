package dialogs;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import shapes.Circle;
import shapes.Point;

public class CircleModifyDialog extends DialogForm {
	private JTextField txtX;
	private JButton btnOK;
	private JButton btnCancel;
	private JButton btnFill;
	private JButton btnStroke;
	private JTextField txtY;
	private JTextField txtRadius;
	private JLabel lblX;
	private JLabel lblY;
	private JLabel lblStrokeColor;
	private JLabel lblFillColor;
	private JLabel lblRange;
	private Circle circle;

	public CircleModifyDialog(Circle circle) {
		this.circle = circle;
		initComponents();
		createEventHandlers();
	}

	public void initComponents() {
		setBounds(100, 100, 400, 350);
		setTitle("Input radius of a circle");
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		lblX = new JLabel("Center    X=");
		lblX.setBounds(46, 33, 97, 23);
		getContentPane().add(lblX);

		lblY = new JLabel("Y=");
		lblY.setBounds(232, 33, 30, 23);
		getContentPane().add(lblY);

		lblStrokeColor = new JLabel("Stroke color");
		lblStrokeColor.setBounds(62, 151, 108, 15);
		getContentPane().add(lblStrokeColor);

		lblFillColor = new JLabel("Fill color");
		lblFillColor.setBounds(92, 212, 67, 15);
		getContentPane().add(lblFillColor);

		lblRange = new JLabel("Radius=");
		lblRange.setBounds(101, 86, 69, 15);
		getContentPane().add(lblRange);

		txtY = new JTextField(Integer.toString(circle.getCenter().getY()));
		txtY.setBounds(267, 30, 78, 30);
		getContentPane().add(txtY);
		txtY.setColumns(10);

		txtRadius = new JTextField(Integer.toString(circle.getRadius()));
		txtRadius.setBounds(170, 79, 78, 30);
		getContentPane().add(txtRadius);

		txtX = new JTextField(Integer.toString(circle.getCenter().getX()));
		txtX.setBounds(141, 30, 78, 30);
		getContentPane().add(txtX);

		btnOK = new JButton("Confirm");
		Border borderOK = new LineBorder(Color.GREEN, 5);
		btnOK.setBorder(borderOK);
		btnOK.setBackground(Color.WHITE);
		btnOK.setBounds(92, 265, 97, 36);
		getContentPane().add(btnOK);

		btnCancel = new JButton("Cancel");
		Border borderCancel = new LineBorder(Color.RED, 5);
		btnCancel.setBorder(borderCancel);
		btnCancel.setBackground(Color.WHITE);
		btnCancel.setBounds(201, 265, 110, 36);
		getContentPane().add(btnCancel);

		btnFill = new JButton("");
		btnFill.setBackground(circle.getFillColor());
		btnFill.setBounds(170, 197, 175, 30);
		getContentPane().add(btnFill);

		btnStroke = new JButton("");
		btnStroke.setBackground(circle.getColor());
		btnStroke.setBounds(170, 136, 175, 30);
		getContentPane().add(btnStroke);
	}

	public void createEventHandlers() {
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					circle.setCenter(new Point(Integer.parseInt(txtX.getText()), Integer.parseInt(txtY.getText())));
					circle.setRadius(Integer.parseInt(txtRadius.getText()));
					circle.setColor(btnStroke.getBackground());
					circle.setFillColor(btnFill.getBackground());
				} catch (NumberFormatException nfe) {
				}
				dispose();
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnStroke.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStroke.setBackground(JColorChooser.showDialog(null, "Choose new Stroke color", circle.getColor()));
			}
		});
		btnFill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnFill.setBackground(JColorChooser.showDialog(null, "Choose new Fill color", circle.getFillColor()));
			}
		});
	}

	public Circle getData() {
		return circle;
	}
}
