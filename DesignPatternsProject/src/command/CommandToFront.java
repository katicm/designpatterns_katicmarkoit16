package command;

import java.util.Collections;

import app.PaintBetaModel;
import shapes.Shapes;

public class CommandToFront implements Command {
	private PaintBetaModel model;
	private int index;
	private Shapes shape;

	public CommandToFront(PaintBetaModel model, Shapes shape) {
		this.model = model;
		this.shape = shape;
	}

	@Override
	public void execute() {
		index = model.getIndex(shape);
		if (index != model.getAll().size() - 1)
			Collections.swap(model.getAll(), index, index + 1);
	}

	@Override
	public void unexecute() {
		if (index != model.getAll().size() - 1)
			Collections.swap(model.getAll(), index + 1, index);
	}

	public String toString() {
		return "ToFront - " + shape;
	}

}