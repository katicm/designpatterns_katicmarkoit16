package command;

import adapter.HexagonAdapter;
import shapes.Circle;
import shapes.Line;
import shapes.Point;
import shapes.Rectangle;
import shapes.Shapes;
import shapes.Square;
import shapes.Triangle;

public class CommandModifyShape implements Command {
	private Shapes oriShape;
	private Shapes newShape;
	private Shapes oldShape;

	public CommandModifyShape(Shapes oriShape, Shapes newShape) {
		this.oriShape = oriShape;
		this.newShape = newShape;
	}

	@Override
	public void execute() {
		if (oriShape instanceof Point) {
			Point oriPoint = (Point) oriShape;
			Point newPoint = (Point) newShape;
			oldShape = oriShape.clone();
			oriPoint.setX(newPoint.getX());
			oriPoint.setY(newPoint.getY());
			oriPoint.setColor(newPoint.getColor());
			oriShape = oriPoint;
		} else if (oriShape instanceof Line) {
			Line oriLine = (Line) oriShape;
			Line newLine = (Line) newShape;
			oldShape = oriShape.clone();
			oriLine.setStart(newLine.getStart());
			oriLine.setEnd(newLine.getEnd());
			oriLine.setColor(newLine.getColor());
			oriShape = oriLine;
		} else if (oriShape instanceof Rectangle) {
			Rectangle oriRect = (Rectangle) oriShape;
			Rectangle newRect = (Rectangle) newShape;
			oldShape = oriShape.clone();
			oriRect.setCorner(newRect.getCorner());
			oriRect.setLength(newRect.getLength());
			oriRect.setWidth(newRect.getWidth());
			oriRect.setColor(newRect.getColor());
			oriRect.setFillColor(newRect.getFillColor());
			oriShape = oriRect;
		} else if (oriShape instanceof Square) {
			Square oriSquare = (Square) oriShape;
			Square newSquare = (Square) newShape;
			oldShape = oriShape.clone();
			oriSquare.setCorner(newSquare.getCorner());
			oriSquare.setLength(newSquare.getLength());
			oriSquare.setColor(newSquare.getColor());
			oriSquare.setFillColor(newSquare.getFillColor());
			oriShape = oriSquare;
		} else if (oriShape instanceof Circle) {
			Circle oriCircle = (Circle) oriShape;
			Circle newCircle = (Circle) newShape;
			oldShape = oriShape.clone();
			oriCircle.setCenter(newCircle.getCenter());
			oriCircle.setRadius(newCircle.getRadius());
			oriCircle.setColor(newCircle.getColor());
			oriCircle.setFillColor(newCircle.getFillColor());
			oriShape = oriCircle;
		} else if (oriShape instanceof Triangle) {
			Triangle oriTri = (Triangle) oriShape;
			Triangle newTri = (Triangle) newShape;
			oldShape = oriShape.clone();
			oriTri.setVertexA(newTri.getVertexA());
			oriTri.setVertexB(newTri.getVertexB());
			oriTri.setVertexC(newTri.getVertexC());
			oriTri.setColor(newTri.getColor());
			oriTri.setFillColor(newTri.getFillColor());
			oriShape = oriTri;
		} else if (oriShape instanceof HexagonAdapter) {
			HexagonAdapter oriHex = (HexagonAdapter) oriShape;
			HexagonAdapter newHex = (HexagonAdapter) newShape;
			oldShape = oriShape.clone();
			oriHex.setCenter(newHex.getCenter());
			oriHex.setR(newHex.getR());
			oriHex.setColor(newHex.getColor());
			oriHex.setFillColor(newHex.getFillColor());
			oriShape = oriHex;
		}
	}

	@Override
	public void unexecute() {
		if (oriShape instanceof Point) {
			Point oriPoint = (Point) oriShape;
			Point oldPoint = (Point) oldShape;
			oriPoint.setX(oldPoint.getX());
			oriPoint.setY(oldPoint.getY());
			oriPoint.setColor(oldPoint.getColor());
			oriShape = oriPoint;
		} else if (oriShape instanceof Line) {
			Line oriLine = (Line) oriShape;
			Line oldLine = (Line) oldShape;
			oriLine.setStart(oldLine.getStart());
			oriLine.setEnd(oldLine.getEnd());
			oriLine.setColor(oldLine.getColor());
			oriShape = oriLine;
		} else if (oriShape instanceof Rectangle) {
			Rectangle oriRect = (Rectangle) oriShape;
			Rectangle oldRect = (Rectangle) oldShape;
			oriRect.setCorner(oldRect.getCorner());
			oriRect.setLength(oldRect.getLength());
			oriRect.setWidth(oldRect.getWidth());
			oriRect.setColor(oldRect.getColor());
			oriRect.setFillColor(oldRect.getFillColor());
			oriShape = oriRect;
		} else if (oriShape instanceof Square) {
			Square oriSquare = (Square) oriShape;
			Square oldSquare = (Square) oldShape;
			oriSquare.setCorner(oldSquare.getCorner());
			oriSquare.setLength(oldSquare.getLength());
			oriSquare.setColor(oldSquare.getColor());
			oriSquare.setFillColor(oldSquare.getFillColor());
			oriShape = oriSquare;
		} else if (oriShape instanceof Circle) {
			Circle oriCircle = (Circle) oriShape;
			Circle oldCircle = (Circle) oldShape;
			oriCircle.setCenter(oldCircle.getCenter());
			oriCircle.setRadius(oldCircle.getRadius());
			oriCircle.setColor(oldCircle.getColor());
			oriCircle.setFillColor(oldCircle.getFillColor());
			oriShape = oriCircle;
		} else if (oriShape instanceof Triangle) {
			Triangle oriTri = (Triangle) oriShape;
			Triangle oldTri = (Triangle) oldShape;
			oriTri.setVertexA(oldTri.getVertexA());
			oriTri.setVertexB(oldTri.getVertexB());
			oriTri.setVertexC(oldTri.getVertexC());
			oriTri.setColor(oldTri.getColor());
			oriTri.setFillColor(oldTri.getFillColor());
			oriShape = oriTri;
		} else if (oriShape instanceof HexagonAdapter) {
			HexagonAdapter oriHex = (HexagonAdapter) oriShape;
			HexagonAdapter oldHex = (HexagonAdapter) oldShape;
			oriHex.setCenter(oldHex.getCenter());
			oriHex.setR(oldHex.getR());
			oriHex.setColor(oldHex.getColor());
			oriHex.setFillColor(oldHex.getFillColor());
			oriShape = oriHex;
		}
	}

	public String toString() {
		return "Modified - " + oldShape + " into " + oriShape;
	}

}
