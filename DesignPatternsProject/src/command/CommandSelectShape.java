package command;

import app.PaintBetaModel;
import shapes.Shapes;

public class CommandSelectShape implements Command {
	private PaintBetaModel model;
	private Shapes shape;

	public CommandSelectShape(PaintBetaModel model, Shapes shape) {
		this.shape = shape;
		this.model = model;
	}

	@Override
	public void execute() {
		model.getAll().get(model.getIndex(shape)).setSelected(true);

	}

	@Override
	public void unexecute() {
		model.getAll().get(model.getIndex(shape)).setSelected(false);
	}

	public String toString() {
		return "Selected - " + shape;
	}

}
