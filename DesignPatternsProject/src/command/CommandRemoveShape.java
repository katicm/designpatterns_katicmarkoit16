package command;

import app.PaintBetaModel;
import shapes.Shapes;

public class CommandRemoveShape implements Command {

	private PaintBetaModel model;
	private Shapes shape;
	private int index;

	public CommandRemoveShape(PaintBetaModel model, Shapes shape) {
		this.model = model;
		this.shape = shape;
	}

	@Override
	public void execute() {
		index = model.getIndex(shape);
		model.remove(shape);
	}

	@Override
	public void unexecute() {
		model.getAll().add(index, shape);
	}

	public String toString() {
		return "Removed - " + shape;
	}

}
