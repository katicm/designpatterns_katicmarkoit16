package command;

import app.PaintBetaModel;
import shapes.Shapes;

public class CommandBringToFront implements Command {
	private PaintBetaModel model;
	private int index;
	private Shapes shape;

	public CommandBringToFront(PaintBetaModel model, Shapes shape) {
		this.model = model;
		this.shape = shape;
	}

	@Override
	public void execute() {
		index = model.getIndex(shape);
		model.getAll().remove(index);
		model.getAll().add(model.getAll().size(), shape);
	}

	@Override
	public void unexecute() {
		model.getAll().remove(shape);
		model.getAll().add(index, shape);
	}

	public String toString() {
		return "BringToFront - " + shape;
	}

}