package command;

import app.PaintBetaModel;
import shapes.Shapes;

public class CommandAddShape implements Command {
	private PaintBetaModel model;
	private Shapes shape;

	public CommandAddShape(PaintBetaModel model, Shapes shape) {
		this.model = model;
		this.shape = shape;
	}

	@Override
	public void execute() {
		model.add(shape);
	}

	@Override
	public void unexecute() {
		model.remove(shape);
	}

	public String toString() {
		return "Added - " + shape;
	}

}
