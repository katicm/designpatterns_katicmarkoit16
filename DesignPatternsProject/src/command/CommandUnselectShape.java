package command;

import app.PaintBetaModel;
import shapes.Shapes;

public class CommandUnselectShape implements Command {
	private PaintBetaModel model;
	private Shapes shape;

	public CommandUnselectShape(PaintBetaModel model, Shapes shape) {
		this.model = model;
		this.shape = shape;
	}

	@Override
	public void execute() {
		model.getAll().get(model.getIndex(shape)).setSelected(false);
	}

	@Override
	public void unexecute() {
		model.getAll().get(model.getIndex(shape)).setSelected(true);

	}

	public String toString() {
		return "Unselected - " + model.get(model.getIndex(shape));
	}
}