package shapes;

import java.awt.Color;
import java.awt.Graphics;

public abstract class Paintable extends Shapes {
	private Color fillColor = Color.WHITE;

	public abstract double surface();

	public abstract double perimeter();

	public abstract void fill(Graphics gc);

	public Color getFillColor() {
		return fillColor;
	}

	public void setFillColor(Color fillColor) {
		this.fillColor = fillColor;
	}

}
