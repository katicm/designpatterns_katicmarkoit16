package shapes;

import java.awt.Color;
import java.awt.Graphics;

public class Rectangle extends Square {
	private int width;

	public Rectangle(Point corner, int length, int width, Color color, Color fillColor) {
		this(corner, length, width);
		setColor(color);
		setFillColor(fillColor);
	}

	public Rectangle(Point corner, int length, int width, Color color) {
		this(corner, length, width);
		setColor(color);
	}

	public Rectangle(Point corner, int length, int width) {
		this.corner = corner;
		this.length = length;
		this.width = width;
	}

	public Rectangle() {

	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public double surface() {
		return length * width;
	}

	public double perimeter() {
		return 2 * length + 2 * width;
	}

	public String toString() {
		return "Rectangle (" + corner.getX() + "," + corner.getY() + ") length=" + getLength() + " width=" + getWidth()
				+ " StrokeColor " + getHexColor(getColor()) + " FillColor " + getHexColor(getFillColor());
	}

	public Line diagonal() {
		return new Line(corner, new Point(corner.getX() + getLength(), corner.getY() + getLength()));
	}

	public boolean equals(Object obj) {
		if (obj instanceof Rectangle) {
			Rectangle temp = (Rectangle) obj;
			if (this.corner.equals(temp.getCorner()) && this.length == temp.getLength()
					&& this.width == temp.getWidth())
				return true;
			else
				return false;
		}
		return false;
	}

	@Override
	public void fill(Graphics gc) {
		gc.setColor(getFillColor());
		gc.fillRect(this.getCorner().getX(), this.getCorner().getY(), this.getLength(), this.getWidth());
	}

	@Override
	public void paint(Graphics gc) {
		fill(gc);
		gc.setColor(getColor());
		gc.drawRect(this.getCorner().getX(), this.getCorner().getY(), this.getLength(), this.getWidth());
		if (isSelected())
			select(gc);
	}

	@Override
	public void select(Graphics gc) {
		gc.setColor(Color.BLUE);
		new Line(new Point(this.corner.getX(), this.getCorner().getY()),
				new Point(this.getCorner().getX() + this.getLength(), this.getCorner().getY())).select(gc);
		new Line(new Point(this.corner.getX() + this.getLength(), this.getCorner().getY()),
				new Point(this.getCorner().getX() + this.getLength(), this.getCorner().getY() + this.getWidth()))
						.select(gc);
		new Line(new Point(this.corner.getX() + this.getLength(), this.getCorner().getY() + this.getWidth()),
				new Point(this.corner.getX(), this.getCorner().getY() + this.getWidth())).select(gc);
		new Line(new Point(this.corner.getX(), this.getCorner().getY() + this.getWidth()),
				new Point(this.corner.getX(), this.getCorner().getY())).select(gc);
	}

	@Override
	public boolean contains(Point pc) {
		if (this.corner.getX() <= pc.getX() && this.corner.getX() + this.getLength() >= pc.getX()
				&& this.corner.getY() <= pc.getY() && this.corner.getY() + this.getWidth() >= pc.getY()) {
			return true;
		}
		return false;
	}

}
