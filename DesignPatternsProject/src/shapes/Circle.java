package shapes;

import java.awt.Color;
import java.awt.Graphics;

public class Circle extends Paintable {
	private Point center;
	private int radius;

	public Circle(Point center, int radius, Color color, Color fillColor) {
		this(center, radius);
		setColor(color);
		setFillColor(fillColor);
	}

	public Circle(Point center, int radius, Color color) {
		this(center, radius);
		setColor(color);
	}

	public Circle(Point center, int radius) {
		this.center = center;
		this.radius = radius;
	}

	public Circle() {

	}

	public Point getCenter() {
		return center;
	}

	public void setCenter(Point center) {
		this.center = center;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public double surface() {
		return radius * radius * Math.PI;
	}

	public double perimeter() {
		return 2 * radius * Math.PI;
	}

	public void moveTo(int x, int y) {
		this.center.moveTo(x, y);
	}

	public void moveFor(int x, int y) {
		this.center.moveFor(x, y);
	}

	public String toString() {
		return "Circle (" + center.getX() + "," + center.getY() + ") r=" + radius + " StrokeColor "
				+ getHexColor(getColor()) + " FillColor " + getHexColor(getFillColor());
	}

	public boolean equals(Object obj) {
		if (obj instanceof Circle) {
			Circle temp = (Circle) obj;
			if (this.center.equals(temp.getCenter()) && this.radius == temp.getRadius())
				return true;
			else
				return false;
		}
		return false;
	}

	public int compareTo(Object obj) {
		if (obj instanceof Circle) {
			Circle temp = (Circle) obj;
			return (int) (this.surface() - temp.surface());
		} else
			return 0;
	}

	@Override
	public void fill(Graphics gc) {
		gc.setColor(getFillColor());
		gc.fillOval(this.getCenter().getX() - this.getRadius(), this.getCenter().getY() - this.getRadius(),
				this.getRadius() * 2, this.getRadius() * 2);
	}

	@Override
	public void paint(Graphics gc) {
		fill(gc);
		gc.setColor(getColor());
		gc.drawOval(this.getCenter().getX() - this.getRadius(), this.getCenter().getY() - this.getRadius(),
				this.getRadius() * 2, this.getRadius() * 2);
		if (isSelected())
			select(gc);

	}

	@Override
	public void select(Graphics gc) {
		gc.setColor(Color.BLUE);
		new Line(new Point(this.getCenter().getX(), this.getCenter().getY() - this.getRadius()),
				new Point(this.getCenter().getX(), this.getCenter().getY() + this.getRadius())).select(gc);
		new Line(new Point(this.getCenter().getX() - this.getRadius(), this.getCenter().getY()),
				new Point(this.getCenter().getX() + this.getRadius(), this.getCenter().getY())).select(gc);
	}

	@Override
	public boolean contains(Point pc) {
		if (this.getCenter().distance(pc) <= this.getRadius()) {
			return true;
		} else
			return false;
	}

}
