package shapes;

import java.awt.Color;
import java.awt.Graphics;

public abstract class Shapes implements Movable, Cloneable {
	private Color color = Color.BLACK;
	private boolean isSelected = false;

	public Shapes(Color color) {
		this.color = color;
	}

	public Shapes() {

	}

	public abstract void paint(Graphics gc);

	public abstract void select(Graphics gc);

	public abstract boolean contains(Point pc);

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public String getHexColor(Color color) {
		int red = color.getRed();
		int green = color.getGreen();
		int blue = color.getBlue();
		String hex = String.format("#%02x%02x%02x", red, green, blue);
		return hex;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public Shapes clone() {
		Shapes clone = null;

		try {
			clone = (Shapes) super.clone();

		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}

		return clone;
	}

}
